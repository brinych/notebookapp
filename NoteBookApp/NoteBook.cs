﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class NoteBook
    {
        //В этом словаре хранятся все записи нашей записной книжки. А каждая отдельно взятая запись хранится в своем отдельном словаре. 
        //Используем allNotes, чтобы задать каждой записи уникальный идентификатор.
        static Dictionary<int, Dictionary<int, string>> allNotes = new Dictionary<int, Dictionary<int, string>>();

        static void Main(string[] args)
        {
            int convertedMenuItem;
            int notesCounter = 0;   //уникальный идентификатор каждой записи в книжке. Все записи будем идентифицировать и обращаться к ним просто по какому-то случайному числу.
            string menuItem;
            while (true)
            {
                NoteBook noteBook = new NoteBook();
                Console.WriteLine("Меню:");
                Console.WriteLine("1. Создать запись");
                Console.WriteLine("2. Редактировать запись");
                Console.WriteLine("3. Удалить запись");
                Console.WriteLine("4. Просмотреть запись");
                Console.WriteLine("5. Все записи");
                Console.WriteLine("6. Выход");
                while (true)
                {
                    Console.Write("Выберите действие, нажав соответствующую цифру на клавиатуре, затем нажмите Enter: ");
                    menuItem = Console.ReadLine();
                    if (!Regex.IsMatch(menuItem, @"^[1-6]$"))
                    {
                        Console.WriteLine("Следует ввести число от 1 до 6!");
                    }
                    else
                    {
                        convertedMenuItem = Convert.ToInt32(menuItem);
                        break;
                    }
                }
                switch (convertedMenuItem)
                {
                    case 1:
                        notesCounter++;
                        CreateNewNote(notesCounter);
                        break;
                    case 2:
                        EditNote();
                        break;
                    case 3:
                        DeleteNote();
                        break;
                    case 4:
                        ReadNote();
                        break;
                    case 5:
                        ShowAllNotes();
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;

                }
                Console.Clear();
            }

        }

        public static void CreateNewNote(int notesCounter)
        {
            Console.Clear();
            Note note = new Note();
            while (true)
            {
                Console.Write("Введите имя:");
                note.Name = Console.ReadLine();
                if (!note.Name.Equals(""))
                {
                    break;
                }
            }

            while (true)
            {
                Console.Write("Введите фамилию:");
                note.Surname = Console.ReadLine();
                if (!note.Surname.Equals(""))
                {
                    break;
                }
            }

            Console.Write("Введите отчество:");
            note.Patronymic = Console.ReadLine();

            while (true)
            {
                Console.Write("Введите номер телефона:");
                note.PhoneNumber = Console.ReadLine();
                if (!note.PhoneNumber.Equals(""))
                {
                    break;
                }
            }

            while (true)
            {
                Console.Write("Введите страну:");
                note.Country = Console.ReadLine();
                if (!note.Country.Equals(""))
                {
                    break;
                }
            }

            Console.Write("Введите дату рождения:");
            note.Birthday = Console.ReadLine();

            Console.Write("Введите название организации:");
            note.Organization = Console.ReadLine();

            Console.Write("Введите должность:");
            note.Post = Console.ReadLine();

            Console.Write("Заметки:");
            note.Notes = Console.ReadLine();

            //Создаем отдельный словарь для каждой записи
            Dictionary<int, string> oneNote = new Dictionary<int, string>();
            oneNote.Add(1, note.Name);
            oneNote.Add(2, note.Surname);
            oneNote.Add(3, note.Patronymic);
            oneNote.Add(4, note.PhoneNumber);
            oneNote.Add(5, note.Country);
            oneNote.Add(6, note.Birthday);
            oneNote.Add(7, note.Organization);
            oneNote.Add(8, note.Post);
            oneNote.Add(9, note.Notes);

            //Добавляем запись в книжку
            allNotes.Add(notesCounter, oneNote);
            Console.WriteLine("Запись создана!");
            Console.WriteLine("Нажмите любую кнопку для продолжения...");
            Console.ReadKey();
        }

        public static void EditNote()
        {
            Note note = new Note();
            string editNum;          //номер изменяемой записи     
            string fieldNum;         //номер изменяемого поля в записи   
            int convertedEditNum;    //номер изменяемой записи
            int convertedFieldNum;   //номер изменяемого поля в записи

            Console.Clear();

            if (allNotes.Count != 0)  //проверяем, есть ли вообще в книжке записи
            {
                Console.Write("Сейчас в записной книжке есть записи с номерами: ");
                foreach (var var in allNotes)
                {
                    Console.Write(var.Key + "  ");
                }

                Console.WriteLine();

                //выбираем запись, которую будем изменять
                while (true)
                {
                    Console.Write("Введите номер записи, которую надо изменить: ");
                    editNum = Console.ReadLine();
                    if (!Regex.IsMatch(editNum, @"^\d+$"))
                    {
                        Console.WriteLine("Введите номер одной из существующих записей!");
                    }
                    else if (!allNotes.Keys.Contains(Convert.ToInt32(editNum)))
                    {
                        Console.WriteLine("Записи с таким номером нет. Повторите попытку!");
                    }
                    else
                    {
                        convertedEditNum = Convert.ToInt32(editNum);
                        break;
                    }
                }

                Console.WriteLine("Выберите поле, которое хотите изменить:");
                Console.WriteLine("1. Имя");
                Console.WriteLine("2. Фамилия");
                Console.WriteLine("3. Отчество");
                Console.WriteLine("4. Номер телефона");
                Console.WriteLine("5. Страна");
                Console.WriteLine("6. Дата рождения");
                Console.WriteLine("7. Организация");
                Console.WriteLine("8. Должность");
                Console.WriteLine("9. Заметки");

                //выбираем поле, которое будем менять
                while (true)
                {
                    Console.Write("Введите соответствующее число: ");
                    fieldNum = Console.ReadLine();
                    if (!Regex.IsMatch(fieldNum, @"^[1-9]$"))
                    {
                        Console.WriteLine("Следует ввести число от 1 до 9!");
                    }
                    else
                    {
                        convertedFieldNum = Convert.ToInt32(fieldNum);
                        break;
                    }
                }

                Console.WriteLine("Меняем значение поля:");
                switch (convertedFieldNum)     //в свиче, собственно, и меняем выбранное поле
                {
                    case 1:
                        {
                            while (true)
                            {
                                Console.Write("Введите новое имя:");
                                note.Name = Console.ReadLine();
                                if (!note.Name.Equals(""))
                                {
                                    break;
                                }
                            }
                            allNotes[convertedEditNum][1] = note.Name;
                            break;
                        }
                    case 2:
                        {
                            while (true)
                            {
                                Console.Write("Введите новую фамилию:");
                                note.Surname = Console.ReadLine();
                                if (!note.Surname.Equals(""))
                                {
                                    break;
                                }
                            }
                            allNotes[convertedEditNum][2] = note.Surname;
                            break;
                        }
                    case 3:
                        {
                            Console.Write("Введите новое отчество:");
                            note.Patronymic = Console.ReadLine();
                            allNotes[convertedEditNum][3] = note.Patronymic;
                            break;
                        }
                    case 4:
                        {
                            while (true)
                            {
                                Console.Write("Введите новый номер телефона:");
                                note.PhoneNumber = Console.ReadLine();
                                if (!note.PhoneNumber.Equals(""))
                                {
                                    break;
                                }
                            }
                            allNotes[convertedEditNum][4] = note.PhoneNumber;
                            break;
                        }
                    case 5:
                        {
                            while (true)
                            {
                                Console.Write("Введите новую страну:");
                                note.Country = Console.ReadLine();
                                if (!note.Country.Equals(""))
                                {
                                    break;
                                }
                            }
                            allNotes[convertedEditNum][5] = note.Country;
                            break;
                        }
                    case 6:
                        {
                            Console.Write("Введите новую дату рождения:");
                            note.Birthday = Console.ReadLine();
                            allNotes[convertedEditNum][6] = note.Birthday;
                            break;
                        }
                    case 7:
                        {
                            Console.Write("Введите новое название организации:");
                            note.Birthday = Console.ReadLine();
                            allNotes[convertedEditNum][7] = note.Organization;
                            break;
                        }
                    case 8:
                        {
                            Console.Write("Введите новую должность:");
                            note.Birthday = Console.ReadLine();
                            allNotes[convertedEditNum][8] = note.Post;
                            break;
                        }
                    case 9:
                        {
                            Console.Write("Введите новую заметку:");
                            note.Birthday = Console.ReadLine();
                            allNotes[convertedEditNum][9] = note.Notes;
                            break;
                        }
                }
                Console.WriteLine("Запись изменена!");
            }
            else
            {
                Console.WriteLine("Записная книжка пуста!");
            }
            Console.WriteLine("Нажмите любую кнопку для продолжения...");
            Console.ReadKey();
        }

        public static void DeleteNote()
        {
            string deleteNum;   //номер записи, которую будем удалять
            Console.Clear();
            if (allNotes.Count != 0)    //проверяем, есть ли вообще в книжке записи
            {
                Console.Write("Сейчас в записной книжке есть записи с номерами: ");
                foreach (var var in allNotes)
                {
                    Console.Write(var.Key + "  ");
                }
                Console.WriteLine();
                while (true)
                {
                    Console.Write("Введите номер записи, которую надо удалить: ");
                    deleteNum = Console.ReadLine();
                    if (!Regex.IsMatch(deleteNum, @"^\d+$"))
                    {
                        Console.WriteLine("Введите номер одной из существующих записей!");
                    }
                    else if (!allNotes.Keys.Contains(Convert.ToInt32(deleteNum)))
                    {
                        Console.WriteLine("Записи с таким номером нет. Повторите попытку!");
                    }
                    else
                    {
                        int convertedNum = Convert.ToInt32(deleteNum);   //тот же номер записи, которую будем удалять
                        allNotes.Remove(convertedNum);
                        break;
                    }
                }
                Console.WriteLine("Запись удалена!");
            }
            else
            {
                Console.WriteLine("Записная книжка пуста!");
            }
            Console.WriteLine("Нажмите любую кнопку для продолжения...");
            Console.ReadKey();
        }

        public static void ReadNote()
        {
            string readNum;  //номер записи, которую будем просматривать
            Console.Clear();
            if (allNotes.Count != 0)    //проверяем, есть ли вообще в книжке записи
            {
                Console.Write("Сейчас в записной книжке есть записи с номерами: ");
                foreach (var var in allNotes)
                {
                    Console.Write(var.Key + "  ");
                }
                Console.WriteLine();
                while (true)
                {
                    Console.Write("Введите номер записи, которую хотите посмотреть: ");
                    readNum = Console.ReadLine();
                    if (!Regex.IsMatch(readNum, @"^\d+$"))
                    {
                        Console.WriteLine("Введите номер одной из существующих записей!");
                    }
                    else if (!allNotes.Keys.Contains(Convert.ToInt32(readNum)))
                    {
                        Console.WriteLine("Записи с таким номером нет. Повторите попытку!");
                    }
                    else
                    {
                        int convertedNum = Convert.ToInt32(readNum);   //тот же номер записи, которую будем просматривать
                        Console.WriteLine("Имя: " + allNotes[convertedNum][1]);
                        Console.WriteLine("Фамилия: " + allNotes[convertedNum][2]);
                        Console.WriteLine("Отчество: " + allNotes[convertedNum][3]);
                        Console.WriteLine("Номер телефона: " + allNotes[convertedNum][4]);
                        Console.WriteLine("Страна: " + allNotes[convertedNum][5]);
                        Console.WriteLine("Дата рождения: " + allNotes[convertedNum][6]);
                        Console.WriteLine("Организация: " + allNotes[convertedNum][7]);
                        Console.WriteLine("Должность: " + allNotes[convertedNum][8]);
                        Console.WriteLine("Заметки: " + allNotes[convertedNum][9]);
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Записная книжка пуста!");
            }
            Console.WriteLine("Нажмите любую кнопку для продолжения...");
            Console.ReadKey();
        }

        public static void ShowAllNotes()
        {
            Console.Clear();
            if (allNotes.Count != 0)    //проверяем, есть ли вообще в книжке записи
            {
                Console.WriteLine("Имя | Фамилия | Номер телефона");
                foreach (var var in allNotes)
                {
                    Console.WriteLine($"{ var.Value[1]} | {var.Value[2]} | {var.Value[4]}");
                }
            }
            else
            {
                Console.WriteLine("Записная книжка пуста!");
            }
            Console.WriteLine("Нажмите любую кнопку для продолжения...");
            Console.ReadKey();
        }
    }
}
