﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class Note
    {
        //Довольно слабая валидация сделана только для тех полей, которые обязательные, то есть, в которых должно быть хоть что-то написано.
        //В поле с номером телефона вводим только цифры, не более 15(максимальный размер номера в Википедии). В остальные поля можно вводить вот все, что угодно.
        string name = "";
        string surname = "";
        string phoneNumber = "";
        string country = "";

        public string Name
        {
            get { return name; }
            set
            {

                if (value.Equals(""))
                {
                    Console.WriteLine("Имя должно содержать хотя бы 1 символ!");
                }
                else
                {
                    name = value;
                }
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {

                if (value.Equals(""))
                {
                    Console.WriteLine("Фамилия должна содержать хотя бы 1 символ!");
                }
                else
                {
                    surname = value;
                }
            }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set
            {
                if (value.Equals(""))
                {
                    Console.WriteLine("Номер телефона должен содержать хотя бы 1 цифру!");
                }
                else if (!Regex.IsMatch(value, @"^\d+$"))
                {
                    Console.WriteLine("Номер телефона должен состоять только из цифр!");
                }
                else if (value.Length > 15)
                {
                    Console.WriteLine("Номер телефона должен содержать не более 15 цифр!");
                }
                else
                {
                    phoneNumber = value;
                }
            }
        }
        public string Country
        {
            get { return country; }
            set
            {

                if (value.Equals(""))
                {
                    Console.WriteLine("Название страны должно содержать хотя бы 1 символ!");
                }
                else
                {
                    country = value;
                }
            }
        }
        public string Patronymic { get; set; }
        public string Birthday { get; set; }
        public string Organization { get; set; }
        public string Post { get; set; }
        public string Notes { get; set; }


    }
}
